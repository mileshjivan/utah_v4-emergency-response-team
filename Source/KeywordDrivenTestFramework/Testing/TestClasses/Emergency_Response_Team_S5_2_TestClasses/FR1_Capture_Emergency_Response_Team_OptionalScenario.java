/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Emergency_Response_Team_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Emergency_Response_Team_S5_2_PageObject.Emergency_Response_Team_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR1-Capture Emergency Response Team v5.2 - Optional Scenario",
        createNewBrowserInstance = false
)

public class FR1_Capture_Emergency_Response_Team_OptionalScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_Capture_Emergency_Response_Team_OptionalScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!UploadSupportingDocument()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Uploadeded a supporting document.");
    }

    public boolean UploadSupportingDocument() {
        //Supporting Documents tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.supporting_tab())) {
            error = "Failed to wait for Supporting Documents tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.supporting_tab())) {
            error = "Failed to click Supporting Documents tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to supporting documents.");
        
        //Add support document - Hyperlink
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.linkbox())) {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.linkbox())) {
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow()) {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.LinkURL())) {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Emergency_Response_Team_PageObject.LinkURL(), getData("Document Link"))) {
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.urlTitle())) {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Emergency_Response_Team_PageObject.urlTitle(), getData("Title"))) {
            error = "Failed to enter '" + getData("Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.urlAddButton())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.urlAddButton())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Emergency_Response_Team_PageObject.iframeXpath())) {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
        
        if(getData("Save supporting documents").equalsIgnoreCase("Yes")){
            //Save supporting documents button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.SaveSupportingDocuments_SaveBtn())) {
                error = "Failed to wait for 'Save supporting documents' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.SaveSupportingDocuments_SaveBtn())) {
                error = "Failed to click on 'Save supporting documents' button.";
                return false;
            }
        }else{
            //Save button        
            if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.SaveButton())) {
                error = "Failed to wait for 'Save' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.SaveButton())) {
                error = "Failed to click on 'Save' button.";
                return false;
            }
        }

         
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Emergency_Response_Team_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Emergency_Response_Team_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Emergency_Response_Team_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
   
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Emergency_Response_Team_PageObject.emergencyResponseTeamRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        return true;
    }
}
