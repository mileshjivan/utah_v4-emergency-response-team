/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Emergency_Response_Team_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Emergency_Response_Team_S5_2_PageObject.Emergency_Response_Team_PageObject;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SKhumalo
 */
@KeywordAnnotation(
        Keyword = "FR2-Capture Response Team v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_Capture_Response_Team_MainScenario extends BaseClass {
    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_Capture_Response_Team_MainScenario() {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Capture_Response_Team_Members()) {
            return narrator.testFailed("Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully captured Response Team Members record.");
    }

    public boolean Capture_Response_Team_Members() {
        
        // Team tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.teamTab())) {
            error = "Failed to wait for Team tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.teamTab())) {
            error = "Failed to click on Team tab";
            return false;
        }
        
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.Response_Team_Add_Button())) {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.Response_Team_Add_Button())) {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");
                
        //Process flow
         pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.RTM_process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
       
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.RTM_process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Full names dropdown        
        if(!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.fullName_Dropdown())){
            error = "Failed to wait for the 'Full names' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.fullName_Dropdown())){
            error = "Failed to click the 'Full names' dropdown.";
            return false;
        }
          
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.impactType_Option(getData("Full names")))) {
            error = "Failed to wait for 'Full names': " + getData("Full names");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.impactType_Option(getData("Full names")))) {
            error = "Failed to click on 'Full names': " + getData("Full names");
            return false;
        }
        narrator.stepPassedWithScreenShot("Full names: " + getData("Full names"));
        
        //Role
        if (!SeleniumDriverInstance.enterTextByXpath(Emergency_Response_Team_PageObject.Role(), getData("Role"))) {
            error = "Failed to '" + getData("Role") + "' into Role field";
            return false;
        }
        narrator.stepPassedWithScreenShot("Role : '" + getData("Role") + "'.");
        
        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.RTM_SaveButton())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.RTM_SaveButton())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Emergency_Response_Team_PageObject.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Emergency_Response_Team_PageObject.validateSave())){
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Emergency_Response_Team_PageObject.validateSave());

        if (!SaveFloat.equals("Record saved")){
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Emergency_Response_Team_PageObject.responseTeamMembersRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        //Close Button
        if(!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.RTM_CloseButton())){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Emergency_Response_Team_PageObject.RTM_CloseButton())){
                error = "Failed to wait for the JSA Assessment close button.";
                return false;
            }
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.RTM_CloseButton())){
            error = "Failed to click the JSA Assessment close button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked JSA Assessment.");
        
        pause(2000);
        //Updated record
        if(!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.RTM_Record(getData("Full names")))){
            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Emergency_Response_Team_PageObject.RTM_Record(getData("Full names")))){
                error = "Failed to wait for the Respond Team Members Record.";
                return false;
            }
        }
        
        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Emergency_Response_Team_PageObject.process_flow())) {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(2000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Emergency_Response_Team_PageObject.process_flow())) {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");
        
        return true;
    }
}
