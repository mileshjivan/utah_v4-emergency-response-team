/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author SKhumalo
 */
public class S5_2_Emergency_Response_Team_TestSuite {

    static TestMarshall instance;

    public S5_2_Emergency_Response_Team_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }
    
    //S5_2_Emergency_Response_Team_QA01S5_2
    @Test
    public void S5_2_Emergency_Response_Team_QA01S5_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\S5_2_Emergency_Response_Team_QA01S5_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 

    //FR1-Capture Emergency Response Team - Main Scenario
    @Test
    public void FR1_Capture_Emergency_Response_Team_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Emergency Response Team v5.2\\FR1-Capture Emergency Response Team - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    } 
    @Test
    public void FR1_Capture_Emergency_Response_Team_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Emergency Response Team v5.2\\FR1-Capture Emergency Response Team - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2-Capture Response Team - Main Scenario
    @Test
    public void FR2_Capture_Response_Team_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Emergency Response Team v5.2\\FR2-Capture Response Team - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR3-View Reports - Main Scenario
    @Test
    public void FR3_View_Reports_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Emergency Response Team v5.2\\FR3-View Reports - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
